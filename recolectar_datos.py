# -*- coding: utf-8 -*-
"""
Created on Fri Feb 17 09:53:13 2017

@author: evimar
"""
import numpy as np
import csv
from sklearn import  linear_model
for i in range(20,26):
    nombre = "X_"+ str(i) +".csv"
    n2= "y_"+ str(i) +".csv"
    X1 = np.genfromtxt(nombre, delimiter=',')
#    y1 = np.genfromtxt(n2, delimiter=',')
    
    
    mean = X1.mean(axis=0)
    
    with open("X.csv", 'a', newline='') as fp:
        a = csv.writer(fp, delimiter=',')
        a.writerow(mean)

#    with open("y.csv", 'a', newline='') as fp:
#        a = csv.writer(fp, delimiter=',')
#        a.writerow([int(y1)])




X = np.genfromtxt("X.csv", delimiter=',')
y = np.genfromtxt("y.csv")

mean = X.mean(axis=0)
std = X.std(axis=0)
S = X - mean
S = S / std

# Create linear regression object
regr = linear_model.LinearRegression()

# Train the model using the training sets
regr.fit(S, y[:-1])

# The coefficients
print('Coefficients: \n', regr.coef_)